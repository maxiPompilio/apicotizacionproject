package APICotizacionProject;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import Interfaces.APICotizacion;

public class APICotizacionStub2Impl implements APICotizacion {

	@Override
	public JsonObject obtenerCotizaciones(String divisaNombre) {
		JsonParser jsonParser = new JsonParser();
    	JsonObject ret;
    	
    	if(divisaNombre.equals("USD")) {
    		ret = jsonParser.parse("{\"ARS\": 83.1024}").getAsJsonObject();
    	} else if (divisaNombre.equals("EUR")) {
    		ret = jsonParser.parse("{\"ARS\": 100.6148}").getAsJsonObject();
    	} else if (divisaNombre.equals("BRL")) {
    		ret = jsonParser.parse("{\"ARS\": 16.9746}").getAsJsonObject();
    	} else {
    		ret = jsonParser.parse("{\"ARS\": -1.00}").getAsJsonObject();
    	}
		
    	return ret;
	}

}
