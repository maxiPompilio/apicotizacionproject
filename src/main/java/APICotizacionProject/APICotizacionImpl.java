package APICotizacionProject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import Interfaces.APICotizacion;

public class APICotizacionImpl implements APICotizacion {

	@Override
	public JsonObject obtenerCotizaciones(String divisaNombre) {
		String url_str = "https://v6.exchangerate-api.com/v6/91d6454f9b8b04de9da49373/latest/" + divisaNombre;
		URL url;
		HttpURLConnection request;
		
		JsonParser jp;
		JsonElement root;
		JsonObject jsonobj = null;
		try {
			url = new URL(url_str);
			request = (HttpURLConnection) url.openConnection();
			request.connect();
			
			jp = new JsonParser();
			root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
			jsonobj = root.getAsJsonObject();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (jsonobj.get("result").getAsString().equals("success")) {
			return jsonobj.get("conversion_rates").getAsJsonObject();
		} else {
			return null;
		}
	}

}
