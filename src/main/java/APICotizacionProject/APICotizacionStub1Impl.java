package APICotizacionProject;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import Interfaces.APICotizacion;

public class APICotizacionStub1Impl implements APICotizacion {

	@Override
	public JsonObject obtenerCotizaciones(String divisaNombre) {
		JsonParser jsonParser = new JsonParser();
    	JsonObject ret;
    	
    	if(divisaNombre.equals("USD")) {
    		ret = jsonParser.parse("{\"ARS\": 82.5024}").getAsJsonObject();
    	} else if (divisaNombre.equals("EUR")) {
    		ret = jsonParser.parse("{\"ARS\": 99.8907}").getAsJsonObject();
    	} else if (divisaNombre.equals("BRL")) {
    		ret = jsonParser.parse("{\"ARS\": 16.4823}").getAsJsonObject();
    	} else {
    		ret = jsonParser.parse("{\"ARS\": -1.00}").getAsJsonObject();
    	}
		
    	return ret;
	}

}
